<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

Interface IStrategyinterface
{
    public function calculate(int $number);
}