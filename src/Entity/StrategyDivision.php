<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use phpDocumentor\Reflection\Types\This;

class StrategyDivision implements IStrategyinterface
{
    private Array $rules ;

    public function __construct(Array $rules){
        $this->rules = $rules;
    }

    public function calculate(int $number)
    {
        $word = "";
        foreach($this->rules as $rule){
            if($number % $rule->getNumber() == 0 ){
                $word .= $rule->getWord();
            }
        }
        return $word == "" ? $number : $word;
    }
}