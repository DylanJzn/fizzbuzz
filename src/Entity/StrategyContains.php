<?php

namespace App\Entity;

class StrategyContains implements IStrategyinterface
{
    private array $rules;

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    public function calculate(int $number)
    {
        $word = "";
        foreach ($this->rules as $rule) {
            if (str_contains($number, $rule->getNumber()) == 1) {
                $word .= $rule->getWord();
            }
        }
        return $word == "" ? $number : $word;
    }
}