<?php

namespace App\Entity;

use App\Repository\StrategyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StrategyRepository::class)]
class Rule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $number;

    #[ORM\Column(type: 'string')]
    private $word;

    public function __construct(int $number, string $word){
        $this->number = $number;
        $this->word = $word;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(int $word): self
    {
        $this->word = $word;

        return $this;
    }
}
