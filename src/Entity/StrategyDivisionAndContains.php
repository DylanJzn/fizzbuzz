<?php

namespace App\Entity;

class StrategyDivisionAndContains implements IStrategyinterface
{
    private Array $rules ;

    public function __construct(Array $rules){
        $this->rules = $rules;
    }

    public function calculate(int $number)
    {
        $word = "";
        foreach($this->rules as $rule){
            if($number % $rule->getNumber() == 0 || str_contains($number, $rule->getNumber()) == 1){
                $word .= $rule->getWord();
            }
        }
        return $word == "" ? $number : $word;
    }
}