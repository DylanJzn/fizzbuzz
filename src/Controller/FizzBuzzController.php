<?php

namespace App\Controller;

use App\Entity\Rule;
use App\Entity\StrategyContains;
use App\Entity\StrategyDivision;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FizzBuzzController extends AbstractController
{
    #[Route('gameDivision/{fizzNumber}/{fizzWord}/{buzzNumber}/{buzzWord}', name: 'fizz_buzz_division')]
    public function division(int $fizzNumber, int $buzzNumber, string $fizzWord, string $buzzWord): Response
    {
        $rule = new Rule($fizzNumber, $fizzWord);
        $rule2 = new Rule($buzzNumber, $buzzWord);
        $strategyDivision = new StrategyDivision([$rule, $rule2]);
        $result = "";

        for($number = 1; $number <= 100; $number++){
            $result .= $strategyDivision->calculate($number)."\n";
        }
        return $this->json([
            'message' => $result,
        ]);
    }

    #[Route('gameContains/{fizzNumber}/{fizzWord}/{buzzNumber}/{buzzWord}', name: 'fizz_buzz_contains')]
    public function contains(int $fizzNumber, int $buzzNumber, string $fizzWord, string $buzzWord): Response
    {
        $rule = new Rule($fizzNumber, $fizzWord);
        $rule2 = new Rule($buzzNumber, $buzzWord);
        $strategyContains = new StrategyContains([$rule, $rule2]);
        $result = "";

        for($number = 1; $number <= 100; $number++){
            $result .= $strategyContains->calculate($number)."\n";
        }
        return $this->json([
            'message' => $result,
        ]);
    }

}
