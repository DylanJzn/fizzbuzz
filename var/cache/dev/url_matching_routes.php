<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/game(?'
                    .'|Division/([^/]++)/([^/]++)/([^/]++)/([^/]++)(*:94)'
                    .'|Contains/([^/]++)/([^/]++)/([^/]++)/([^/]++)(*:145)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        94 => [[['_route' => 'fizz_buzz_division', '_controller' => 'App\\Controller\\FizzBuzzController::division'], ['fizzNumber', 'fizzWord', 'buzzNumber', 'buzzWord'], null, null, false, true, null]],
        145 => [
            [['_route' => 'fizz_buzz_contains', '_controller' => 'App\\Controller\\FizzBuzzController::contains'], ['fizzNumber', 'fizzWord', 'buzzNumber', 'buzzWord'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
